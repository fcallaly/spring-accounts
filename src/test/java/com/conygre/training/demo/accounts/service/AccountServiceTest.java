package com.conygre.training.demo.accounts.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.conygre.training.demo.accounts.entity.Account;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AccountServiceTest {
    
    @Autowired
    AccountService testAccountService;

    @Test
    public void insertAndCountTest(){
        System.out.println("Num Accounts: " + testAccountService.countAccounts());
        testAccountService.insertAccount(new Account("Frank", 100000));

        System.out.println("Num Accounts: " + testAccountService.countAccounts());
        assertEquals(1, testAccountService.countAccounts());
    }
}